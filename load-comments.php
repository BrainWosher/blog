<?php
require "db.php";
$data = $_POST;
$comments = R::findAll('comments'); ?>
<?php foreach ($comments as $comment) { ?>
    <div class="comments__item">
        <div class="comments__author"><?= $comment->author_name ?></div>
        <div class="comments__message"><?= $comment->message ?></div>
        <div class="comments__date"><?= $comment->date ?></div>
    </div>
<?php }; ?>
<?php
require "db.php";
$data = $_POST;

if (isset($data['submit'])) {
    $errors = array();
    if ($data['message'] == '') {
        $errors[] = 'Введите текст комментария';
    }
    if (empty($errors)) { //проверка на наличие ошибок
        //если нет ошибок - регистрируем
        $comments = R::dispense('comments');
        $comments->author_name = $_SESSION['logged_user']->login;
        $comments->message = $data['message'];
        $comments->date = date("d.m.Y");
        R::store($comments);
        header("Location: /index.php");
        echo '<div style="color: green;">Ваш комментарий успешно добавлен!</div><hr>';
    } else {
        echo '<div style="color: red;">' . array_shift($errors) . '</div><hr>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Blog</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&display=swap" rel="stylesheet">
</head>

<body>
    <?php if (isset($_SESSION['logged_user'])) : ?>
        <div class="navigation">
            <div class="navigation__main">
                <a class="navigation__link" href="/">home</a>
            </div>
            <div class="navigation__side">
                <div class="navigation__status">
                    Привет, <?php echo $_SESSION['logged_user']->login; ?>!<br />
                </div>
                <a class="navigation__link" href="/article.php">Add Articles</a>
                <div class="logout"><a class="navigation__link" href="/logout.php">Выйти</a></div>
            </div>
        </div>
        <div class="wrapper">
            <div class="articles">
                <!-- получаем все статьи из бд -->
                <?php $articles = R::findAll('articles'); ?>
                <?php foreach ($articles as $article) { ?>
                    <div class="article">
                        <div class="erticle__main">
                            <div class="article__title"><?= $article->title ?></div>
                            <div class="article__content"><?= $article->content ?></div>
                        </div>
                        <div class="article__side">
                            <div class="article__author"><?= $article->author_name ?></div>
                            <div class="article__date"><?= $article->date ?></div>
                        </div>
                    </div>
                    <div class="comments">
                        <div class="comments__label"> Комментарии к статье</div>
                        <div class="comments__main"></div>
                        <script>
                            function loadComments(){
                                $.get( '/load-comments.php', function (data) {
                                    if ( data == '') {
                                        alert('Комментарии отсутствуют! Добавте первый комментарий.');
                                    } else {
                                        $('.comments__main').append(data);
                                    }
                                });
                            }
                        </script>
                        <button class="button" type="submit" onclick="loadComments()">Показать комментарии</button>
                    </div>
                    <!-- форма добавления комментария -->
                    <button class="comment-button button" type="submit">Комментировать</button>
                    <div class="comment-form">
                        <form action="index.php" method="POST">
                            <p>
                                <div class="comment-form__label">Текст комментария</div>
                                <textarea rows="3" cols="22" name="message"><?php echo @$data['message']; ?></textarea>
                            </p>
                            <p>
                                <button class="comment-button button" type="submit" name="submit">Отправить комментарий</button>
                            </p>
                        </form>
                    </div>
                    <!---->
                <?php }; ?>
            </div>
        </div>
    <?php else : ?>
        <div class="navigation">
            <div class="navigation__main">
                <a class="navigation__link" href="/">home</a>
            </div>
            <div class="navigation__side">
                <div class="navigation__status">Вы не авторизованы</div>
                <a class="navigation__link" href="/login.php">sign in</a>
                <a class="navigation__link" href="/signup.php">registration</a>
            </div>
        </div>
        <div class="wrapper">
            <div class="prompt">Для дальейшей работы с сайтом, вам необходимо зарегистрироваться, либо войти под своим пользователем</div>
        </div>
    <?php endif; ?>
    <div class="footer">Created by Sergey Prokopenko. 2019</div>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>

</html>
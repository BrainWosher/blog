<?php
require "db.php";
$data = $_POST;
if (isset($data['do_signup'])) { //если была нажата кнопка do_signup, тогда происходит регистрацию
    //здесь происходит регистрация
    $errors = array();
    if (trim($data['login']) == '') {
        $errors[] = 'Введите логин';
    }
    if (trim($data['email']) == '') {
        $errors[] = 'Введите email';
    }
    if ($data['password'] == '') {
        $errors[] = 'Введите пароль';
    }
    if ($data['password_2'] != $data['password']) {
        $errors[] = 'Повторный пароль введен не верно';
    }
    if (R::count('users', "login = ?", array($data['login'])) > 0) {
        $errors[] = 'Пользователь с таки логином уже зарегистрирован!';
    }
    if (R::count('users', "email = ?", array($data['email'])) > 0) {
        $errors[] = 'Пользователь с таки адресом электронной почты уже зарегистрирован!';
    }
    if (empty($errors)) { //проверка на наличие ошибок
        //если нет ошибок - регистрируем
        $user = R::dispense('users');
        $user->login = $data['login'];
        $user->email = $data['email'];
        $user->password = password_hash($data['password'], PASSWORD_DEFAULT);
        R::store($user);
        echo '<div style="color: green;">Вы успешно зарегистрированы! Можете перейти на <a href="/">главную </a> страницу</div><hr>';
    } else {
        echo '<div style="color: red;">' . array_shift($errors) . '</div><hr>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add articles</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&display=swap" rel="stylesheet">
</head>

<body>
    <div class="navigation">
        <div class="navigation__main">
            <a class="navigation__link" href="/">home</a>
        </div>
    </div>
    <div class="wrapper">
        <form class="form__signup" action="/signup.php" method="POST">
            <p>
                <div class="signup__label">Ваш логин</div>
                <input type="text" name="login" value="<?php echo @$data['login']; ?>">
            </p>
            <p>
                <div class="signup__label">Ваш Email</div>
                <input type="email" name="email" value="<?php echo @$data['email']; ?>">
            </p>
            <p>
                <div class="signup__label">Ваш пароль</div>
                <input type="password" name="password" value="<?php echo @$data['password']; ?>">
            </p>
            <p>
                <div class="signup__label">Подтвердите ваш пароль</div>
                <input type="password" name="password_2" value="<?php echo @$data['password_2']; ?>">
            </p>
            <p>
                <button class="comment-button button" type="submit" name="do_signup">Зарегистрироваться</button>
            </p>
        </form>
    </div>
    <div class="footer">Created by Sergey Prokopenko. 2019</div>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>

</html>
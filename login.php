<?php
require "db.php";
$data = $_POST;
if (isset($data['do_login'])) {
    $errors = array();
    $user = R::findOne('users', 'login = ?', array($data['login']));
    if ($user) {
        //логин существует 
        if (password_verify($data['password'], $user->password)) {
            //пароль верный, логинимся и редиректим пользователя на главную страницу
            $_SESSION['logged_user'] = $user;
            header('location:/');
        } else {
            $errors[] = "Неверно введен пароль!";
        }
    } else {
        $errors[] = 'Пользователь с таким логином не найден!';
    }
    if (!empty($errors)) { //проверка на наличие ошибок
        echo '<div style="color: red;">' . array_shift($errors) . '</div><hr>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add articles</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&display=swap" rel="stylesheet">
</head>

<body>

    <div class="navigation">
        <div class="navigation__main">
            <a class="navigation__link" href="/">home</a>
        </div>
    </div>
    <div class="wrapper">
        <form class="form__login" action="/login.php" method="POST">
            <p>
                <div class="login__label">Логин</div>
                <input type="text" name="login" value="<?php echo @$data['login']; ?>">
            </p>
            <p>
                <div class="login__label">Пароль</div>
                <input type="password" name="password" value="<?php echo @$data['password']; ?>">
            </p>
            <p>
                <button class="comment-button button" type="submit" name="do_login">Войти</button>
            </p>
        </form>
    </div>

    <div class="footer">Created by Sergey Prokopenko. 2019</div>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>

</html>
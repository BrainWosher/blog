<?php
require "db.php";
$data = $_POST;

if (isset($data['do_signup'])) {
    $errors = array();
    if ($data['title'] == '') {
        $errors[] = 'Введите заголовок статьи';
    }
    if ($data['content'] == '') {
        $errors[] = 'Введите контент статьи';
    }
    if (empty($errors)) { //проверка на наличие ошибок
        //если нет ошибок - регистрируем
        $articles = R::dispense('articles');

        $articles->author_name = $_SESSION['logged_user']->login;
        $articles->title = $data['title'];
        $articles->content = $data['content'];
        $articles->date = date("d.m.y");

        R::store($articles);
        header("Location: /index.php");
        echo '<div style="color: green;">Вы успешно опубликовали статью!</div><hr>';
    } else {
        echo '<div style="color: red;">' . array_shift($errors) . '</div><hr>';
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Add articles</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400,700&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,400,700&display=swap" rel="stylesheet">
</head>

<body>
    <?php if (isset($_SESSION['logged_user'])) : ?>
        <div class="navigation">
            <div class="navigation__main">
                <a class="navigation__link" href="/">home</a>
            </div>
            <div class="navigation__side">
                <div class="navigation__status">
                    Привет, <?php echo $_SESSION['logged_user']->login; ?>!<br />
                </div>
                <a class="navigation__link" href="/article.php">Add Articles</a>
                <div class="logout"><a class="navigation__link" href="/logout.php">Выйти</a></div>
            </div>
        </div>
        <div class="wrapper">
            <!-- Форма создания статьи -->
            <form action="" method="POST">
                <p>
                    <div class="article__label">Заголовок статьи</div>
                    <input type="text" name="title" value="<?php echo @$data['title']; ?>">
                </p>
                <p>
                    <div class="article__label">Текст статьи</div>
                    <textarea rows="5" cols="40" name="content"><?php echo @$data['content']; ?></textarea>
                </p>
                <button class="comment-button button" type="submit" name="do_signup">Отправить</button>

            </form>
            <!---->
        </div>
    <?php else : ?>
        <div class="navigation">
            <div class="home"><a href="/">home</a></div>
            <div class="authentication">
                <div class="authentication__status">
                    Вы не авторизованы<br />
                </div>
                <div class="authentication__login"><a href="/login.php">sign in</a></div>
                <div class="authentication__registratiom"><a href="/signup.php">registration</a></div>
            </div>
        </div>
        
        <div class="wrapper"></div>
    <?php endif; ?>
    <div class="footer">Created by Sergey Prokopenko. 2019</div>
    <link rel="stylesheet" href="style.css">
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="main.js"></script>
</body>

</html>